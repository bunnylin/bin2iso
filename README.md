bin2iso 2.0
-----------

A commandline tool for converting `bin` + `cue` files into `iso` image files and
separate `wav` audio tracks. This was originally by Bob Doiron, but he moved on
after version 1.9b. Therefore I am adding some further maintenance fixes in this repo.

Original source locations, possibly inaccessible now:
- http://xpt.sourceforge.net/techdocs/media/video/dvdvcd/dv09-NonIsoCDFormats/ar01s10.html
- http://users.eastlink.ca/~doiron/bin2iso/linux/
- http://users.andara.com/~doiron
- http://slackware.uk/sbosrcarch/by-name/system/bin2iso/

Note that `mdf` and `img` files are also raw binary disk images, so bin2iso will
work on them as well. However, although bin2iso can try to auto-generate a `cue` file
from the image file, it's usually best to use the original accompanying `cue/mds/ccd` file
to get exact track indexes. Bin2iso only understands `cue` files, so please convert
`mds/ccd` to `cue` first. Possible tools for that: `ccd2cue` and `mdf2iso`.

Discjuggler `cdi` images are not raw and won't convert well directly. Possible tools
for those: `cdi2iso` and `iat`. If a `cdi` has audio tracks and those tools only produced
a big `iso` file, `bin2iso` may be able to generate an accurate, extractable `cue` from
that big `iso`.


### Usage

Convert a `cue` pointing to a valid `bin`:
```
bin2iso <cuefile>
```

Try to generate a `cue` file from a cueless `bin`:
```
bin2iso <cuefile> -c <binfile>
```

All parameters:
```
bin2iso <cuefile> [<output dir>] [-napib] [-t X]

Where:
<cuefile>     The .cue file that belongs to the image file being converted.
<output dir>  The output directory (defaults to current dir).
-n --nogaps   Discard any data in 'gaps' between tracks.
-a --gaps     Save 'gaps' only if they contain notable non-zero data.
              Looks for more than 1/2 block of non-zeroes (588 values).
              Without -n or -a, by default all gap data is saved.
-p --pregaps  Don't convert pregaps to postgaps, save as is.
-t --track=X  Extracts only the X'th track.
-i --inplace  Performs the conversion 'in place'; ie. truncates the binfile
              after each track is created to minimize space usage.
              (can't be used with -t)
-b --nob      Do not use overburn data past 334873 sectors.
              This of course presumes that the data is not useful.
-c --cuefrom=<binfile>
              Attempts to create a <cuefile> from an existing <binfile>.
-l --level=X  When creating a cuefile, split audio tracks when many sectors
              in a row are below this volume (RMS) level. Default: 80
-w --width=X  When creating a cuefile, split audio tracks when this many
              sectors are below the RMS limit. 75 = 1 second. Default: 48
```


### Building

Run `make`.

Building on Windows: Set up msys32 with mingw32, copy `bin2iso.c` and
`Makefile` into your mingw32 home directory, which will look something like
`C:\msys32\home\<username>\`. Run mingw32. In the mingw32 window, run `make`.

Or you can use the prebuilt exe:
- [bin2iso.exe](https://mooncore.eu/filus/bin2iso.exe) (2.0, 39424 bytes, sha256 ae518599879396ad84eebdba9bc04d9decac5d5ef25b36f9449d1e436a772895)
- [bin2iso19d.exe](https://mooncore.eu/filus/bin2iso19d.exe) (1.9d, 37376 bytes, sha256 4b341a8c7921f645c1c3f1851be2daa10f9acb103a4f8f93987f72fa84b2fdca)
- [bin2iso19c.exe](https://mooncore.eu/filus/bin2iso19c.exe) (1.9c, 34304 bytes, sha256 d5e1340b814c381bca79922e3c2261cf5e46a1af9a24ef0c0dd290fe78ca4191)


### About automatic cue generation

If a disk image only has data files and no audio, a `cue` only needs to indicate
the single data track's format, which bin2iso can normally guess correctly.

For images with multiple additional audio tracks, there is no obvious defined marker
between the tracks in the binary data, so bin2iso tries to guess where the track
splits are by looking for periods of silence. By convention, audio tracks are supposed
to be separated by 2 seconds of complete silence, which most disks respect. However,
a few games have audio tracks with less than 1 second of silence between tracks, and
in worst cases no silence at all. This makes track autodetection nearly impossible.

You can use bin2iso's `-l` and `-w` switches to finetune the split detection algorithm,
but ultimately you may have to edit the generated `cue` file manually to get correct
track splits.


### License

The original author did not specify an explicit license, but it seems this tool was
released in a spirit of free sharing, so can be considered freeware, or an implied WTFPL.
